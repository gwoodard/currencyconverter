# Currency Converter: Android Mobile Application #

### **About:** ###

Android mobile application that will convert dollars to euros interchangeably along with showing the current device's time in a analog clock format. The mobile application was built as a challenge to see how the app development process works. It was released in March of 2012 and currently available in the Google Play Store: https://play.google.com/store/apps/details?id=com.CurrencyConverter.Test 


### **IDE:**
Eclipse

### **Input:** ###
Integer Denomination

### **Language:** ###
Java